package com.example.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.entity.Car;
import com.example.demo.entity.Driver;
import com.example.demo.entity.Podium;
import com.example.demo.entity.Race;
import com.example.demo.entity.Track;
import com.example.demo.repository.DriverRepository;
import com.example.demo.service.CarServiceImpl;
import com.example.demo.service.DriverServiceImpl;
import com.example.demo.service.TrackServiceImpl;

@Component
public class PantallaCrearJugador {
	
	Scanner reader = new Scanner(System.in);
		
	@Autowired
	private DriverServiceImpl driverService;
	
	@Autowired
	private CarServiceImpl carService;
	
	@Autowired
	private TrackServiceImpl trackService;
	
	public void guardarJugador() {
		
		System.out.println("CREAR JUGADOR\n");
		System.out.println("Escribe el nombre de tu jugador\n");
		
		String nombreJugador;
		nombreJugador = reader.next();
		
		//Consultar último id y agregar 1
		
		int nextIdDriver = driverService.latestIdDriver() + 1;
		
		Driver driver = new Driver(nextIdDriver, nombreJugador); //Ajustar id
		guardarConductor(driver, nombreJugador);
		
	}
	
	public void guardarConductor(Driver driver, String name_player) {
		
		System.out.println("SELECCIÓN DE CARRO\n");
		
		List<Car> lista = new ArrayList<Car>();
		lista = carService.findAll();
		
		for (int i=0;i<lista.size();i++) {
			System.out.println(lista.get(i));
		}
		
		System.out.println("Escribe el número del carro con el que desas jugar\n");
		
		Integer numberCar;
		numberCar = reader.nextInt();
		
		// se guardan los datos del conductor
		
		driver.setIdCar(numberCar);
		driverService.Save(driver);
		
		System.out.println("Tu selección de carro se ha guardado.\n");
		mostrarInterfazPrincipal();		
	}
	
	public void mostrarInterfazPrincipal() {
		
		System.out.println("INTERFAZ PRINCIPAL\n");
		System.out.println("1. CREAR JUGADOR\n");
		System.out.println("2. JUEGO NUEVO\n");
		System.out.println("3. SALIR\n");
		System.out.println("Escribe el número de la opción que deseas elegir\n");
		
		int opcionPrincipal = 0;
		opcionPrincipal = reader.nextInt();
		
		switch(opcionPrincipal) {
		case 1: guardarJugador();
		case 2: crearJuego();
		break;
		}
	}
	
	public void crearJuego() {
		
		System.out.println("JUEGO NUEVO\n");
		
        System.out.println("Escribe la cantidad de conductores que van a participar en la carrera\n");
		
		int cantidadConductores = 0;
		cantidadConductores = reader.nextInt();
		
        System.out.println("SELECCIÓN DE CONDUCTORES\n");
        
        List<String> listaDriversNames = new ArrayList<String>();
		
        for (int i=1;i<=cantidadConductores;i++) {
        	
        	System.out.println("Escriba el nombre del conductor Número " + i + "\n");
        	String nameConductor;
        	nameConductor = reader.next();
        	listaDriversNames.add(nameConductor);
        }
        
        System.out.println("SELECCIÓN DE CONDUCTORES EXITOSA\n");
        
        System.out.println("INGRESA LA DISTANCIA DE LAS PISTAS EN KILOMETROS\n");
        int distanciaPista = 0;
		distanciaPista = reader.nextInt();
		
		List<Track> listaPistas = trackService.findAll();
		for (Track pista : listaPistas) {
			pista.setDistanceTrack(distanciaPista);
			trackService.Save(pista);
		}
		
		System.out.println("DISTANCIA DE PISTAS DEFINIDA! CADA PISTA TENDRÁ UNA DISTANCIA DE : " + distanciaPista + " KM. \n");
		        
		System.out.println("EL JUEGO YA SE HA CONFIGURADO, AHORA SI VAMOS A JUGAR!\n");
		
		
		//ejecutar sql nativo
		int conteopista= trackService.countTrack();
		
		       
        ejecutarJuego(cantidadConductores, distanciaPista, listaDriversNames, conteopista);
        
        
	}
	
	
	public void ejecutarJuego(int cantidadConductores, int distanciaPista, List<String> listaNombreConductores, int conteoPista) {
		
		recorrerPistas(conteoPista, cantidadConductores, listaNombreConductores, distanciaPista);
	}
	
	
    public void recorrerConductores(List <Race> listRace, int metrosAcumulados[]) {
    	
  
		
    	for (int i=0;i<cantidadConductores;i++) {
    		
    		int numeroAleatorio = (int) (Math.random()*6+1);
    		int metrosAvanzados = numeroAleatorio*100;
    		metrosAcumulados[i]= metrosAcumulados[i]+metrosAvanzados;
    		
    		
			System.out.println("El conductor " + listaNombreConductores.get(i) + "sacó " + numeroAleatorio + " con el dado, avanza " + metrosAvanzados + " metros, lleva recorrido " + metrosAcumulados[i] + " metros de la pista actual hasta el momento" );
		}
		
	}
    
    public void recorrerTurnosPorMetros( int conversionDistanciaMetros, List<String> listaNombreConductores, int cantidadConductores) {

    	Podium podio = new Podium();
    	List<Race> listaDriver = new ArrayList<Race>();
    	
    	
    	
    	int metrosAcumulados[] = new int[listaNombreConductores.size()];
    	while (podio.getIdThirdPlace() == 0) {
    		recorrerConductores(listaDriver, metrosAcumulados);
    		for(int i=0;i<metrosAcumulados.length;i++) {
    			if(metrosAcumulados[i] >= conversionDistanciaMetros) {
    				if(podio.getIdFirstPlace() == 0)
    					podio.setIdFirstPlace(i);//listaNombreConductores.get(i));
    				else if(podio.getIdSecondPlace() == 0)
    					podio.setIdSecondPlace(i);//listaNombreConductores.get(i);
    				else if(podio.getIdThirdPlace() == 0)
    					podio.setIdThirdPlace(i);//listaNombreConductores.get(i);
    			}
    		}
    	}
    	
    	//Guardar podio BD
    	//Imprimir podio
	}
    
    public void recorrerPistas(int conteoPistas, int cantidadConductores, List<String> listaNombreConductores, int distanciaPistaKilometros) {
    	
    	int conversionDistanciaMetros= distanciaPistaKilometros*1000;
    	
        for (int i=0;i<conteoPistas;i++) {
    		
    		    		recorrerTurnosPorMetros(conversionDistanciaMetros, listaNombreConductores, cantidadConductores );
    		}
		
        //Final de carrera
	}
	
	
	
	
	
	
	
	
}