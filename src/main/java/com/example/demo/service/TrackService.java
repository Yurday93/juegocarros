package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.entity.Track;

public interface TrackService {
	
	public List<Track> findAll();
	
	public Page<Track> findAll(Pageable pageable);
	
	public Optional<Track> findById(Long id);
	
	public Track Save(Track track);
	
	public void deleteById(Long id);
	
	public void updateDistanciaPista(int longitudPista);
	
	public int countTrack();

}