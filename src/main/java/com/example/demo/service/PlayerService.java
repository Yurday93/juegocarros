package com.example.demo.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.entity.Player;

public interface PlayerService {

	public Iterable<Player> findAll();
	
	public Page<Player> findAll(Pageable pageable);
	
	public Optional<Player> findById(Long id);
	
	public Player Save(Player player);
	
	public void deleteById(Long id);


}
