package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Podium;

@Repository
public interface PodiumRepository extends JpaRepository<Podium, Long> {

}
