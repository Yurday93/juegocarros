package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Lane;


@Repository
public interface LaneRepository extends JpaRepository<Lane, Long>{

}
