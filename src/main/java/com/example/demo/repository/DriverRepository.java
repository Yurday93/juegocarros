package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Driver;

	
    @Repository
	public interface DriverRepository extends JpaRepository<Driver, Long>{
    	
    	@Query(value= "SELECT MAX(id_player) AS id FROM driver", nativeQuery = true)
    	public int latestIdDriver();

}